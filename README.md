# FaceAnswer Quiz App
FaceAnswer is an interactive quiz application that allows users to answer questions using their facial movements. By utilizing the front camera of your phone, this app detects the user's face movements and interprets them as answers to the quiz questions. The application is developed in Swift and employs various features to enhance the user experience.

## preview
<p float="left">
  <img src="screenshots/entry.PNG" width="300" />
  <img src="screenshots/type.PNG" width="300" />
  <img src="screenshots/game1.PNG" width="300" />
</p>
<p float="left">
  <img src="screenshots/gam2.PNG" width="300" />
  <img src="screenshots/scoreboard.PNG" width="300" />
</p>

## Features

Face-Movement Answers: Users can answer questions by tilting their face horizontally or vertically. The app utilizes the front camera of the device to detect these movements and interpret them as answers.

Multiple Choice Questions: Each question in the quiz has two answer options for users to select from. The questions are randomly selected from a pool of at least 20 questions, covering various categories such as Math, History, and more.

Answer by moving your head: If you move your head up and down you pick the first option, if your move your head right and left you pick second option
Time Limit: Users have a limited amount of time to provide an answer. The remaining time is displayed on the screen, creating a sense of urgency and excitement.
Revealing Correct Answer: When the timer runs out, the app displays the correct answer to the user. This helps users learn and improve their knowledge while playing the game.
Audio Feedback: Different sound effects are played for correct, wrong, and timed-out answers. This auditory feedback enhances the user experience and adds an element of gamification.

Score Calculation: At the end of the 10-question quiz, users can view their score. Also they may visit the scoreboard. Their last score in the ranking will get yellow background.

User Profile: Users can set an username, which should consist of 2 to 15 characters and should not include emojis. This username is associated with their game scores and allows them to track their progress over time.

Development Side


## Language
The codebase of FaceAnswer is written in Swift, ensuring compatibility with iOS devices.

## Architecture
VIPER design pattern is used as architecture of the project.

## Database
CoreData is used.



